
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function vnosPodatkov() {
    var height = document.querySelector("#addHeight").value;
    var weight = document.querySelector("#addWeight").value;
    var bodyFat = document.querySelector("#addBodyFat").value;
    var heartRate = document.querySelector("#addHeartRate").value;
    var systolic = document.querySelector("#addSystolic").value;
    var diastolic = document.querySelector("#addDiastolic").value;
    
    var heightInInches = ((height*0.01)/0.3048)*12;
    var leanMassPercentage = 100 - bodyFat;
    
    var bloodPressure = systolic + "/" + diastolic + " mm Hg"
    var BMI = weight/((height*0.01)*(height*0.01));
    var leanMass = weight*(1.0-(bodyFat/100));
    var FFMI = (leanMass/2.2)/(((heightInInches)*0.0254)*((heightInInches)*0.0254))*2.2046;
    var adjustedFFMI = FFMI + (6.1*(1.8-(heightInInches*0.0254)));
    
    document.querySelector("#getBloodPressure").value = bloodPressure;
    document.querySelector("#getBMI").value = BMI.toFixed(1);
    document.querySelector("#getLeanMass").value = leanMass.toFixed(1) + " kg";
    document.querySelector("#getFFMI").value = FFMI.toFixed(1);
    document.querySelector("#getAdjustedFFMI").value = adjustedFFMI.toFixed(1);
    
    
    // GRAF ZA KRVNI TLAK
    var ctx = document.getElementById('pressureChart').getContext('2d');
    var chart = new Chart(ctx, {
                    
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset "Average Systolic" #ff8a82
    data: {
        labels: ["Systolic Blood Pressure", "Average Systolic", "Diastolic Blood Pressure", "Average Diastolic"],
        datasets: [{
            backgroundColor: ['#a3c6ff','#ff8a82','#a3c6ff','#ff8a82'],
            borderColor: ['#a3c6ff','#a3c6ff','#a3c6ff','#a3c6ff'],
            data: [systolic,120,diastolic,80],
        }]
    },


    // Configuration options go here
    options: {
        legend: { display: false },
            title: {
            display: true,
            text: 'Blood Pressure'
        },
        
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true,
                    max: 200,
                    min: 0
                }
            }]
        },
    }
    });
    // KONC GRAFA ZA KRVNI TLAK
    
    
    // GRAF ZA BODY FAT 
    var ctx = document.getElementById('bodyFatChart').getContext('2d');
    var chart = new Chart(ctx, {
                    
    // The type of chart we want to create
    type: 'doughnut',

    // The data for our dataset "Average Systolic" #ff8a82
    data: {
        labels: ["Lean Body Mass", "Body Fat"],
        datasets: [{
            backgroundColor: ['#a3c6ff','#ff8a82'],
            borderColor: ['#a3c6ff','#ff8a82'],
            data: [leanMassPercentage,bodyFat],
        }]
    },


    // Configuration options go here
    options: {
        legend: { display: false },
            title: {
            display: true,
            text: 'Body Fat %'
        },
    }
    });
    // KONC GRAFA ZA BODY FAT
    
}

// EHR STVARI

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();
    
    var name = $("#addName").val();
    var age = $("#addAge").val();
    var height = $("#addHeight").val();
    var weight = $("#addWeight").val();
    var bodyFat = $("#addBodyFat").val();
    var heartRate = $("#addHeartRate").val();
    var systolic = $("#addSystolic").val();
    var diastolic = $("#addDiastolic").val();


	if (!name || !age || !height || !weight || !bodyFat || !heartRate || !systolic || !diastolic) {
		
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: name,

		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#addName").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#addName").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

//EHR TEMPLATE OSEBE

$(document).ready(function() {

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiPodatke').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split(",");
		$("#addName").val(podatki[0]);
		$("#addAge").val(podatki[1]);
		$("#addHeight").val(podatki[2]);
		$("#addWeight").val(podatki[3]);
		$("#addBodyFat").val(podatki[4]);
		$("#addHeartRate").val(podatki[5]);
		$("#addSystolic").val(podatki[6]);
		$("#addDiastolic").val(podatki[7]);
		vnosPodatkov();
	});
});
